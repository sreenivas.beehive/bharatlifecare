(function($) {

  "use strict";

  var wporganic = {

    loader: function(){

        $(window).on('load', function() {

            $('.status').fadeOut();
            $('.preloader').delay(300).fadeOut('slow'); 
        });
    },

    slider_one : function(){

        if( $('.slider-one .slider-carsoule').length ){

            $('.slider-one .slider-carsoule').owlCarousel({

                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 10000,
                smartSpeed: 1000,
                nav: false,
                dots: false,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            });
        }
    },

    tabs: function( i ){

        if( $(i).length ){

            $( i+' a' ).on( 'click', function(e){

                $(i).map( function(){ $( this ).removeClass( 'active' ); });
                $( this ).parent().addClass( 'active' );
                e.preventDefault();

            } );
        }
    },

    footer_gallery: function(){

        if( $( '.footer-gallery' ).length ){

            $('.footer-gallery').magnificPopup({

                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-img-mobile',
                image: { verticalFit: true }
            });
        }
    },

    history_tab: function(){

        if( $( '#timeline' ).length  ){

            if( $( '.dot' ).length ){

                $( '.dot' ).map( function(){

                    var i = $( this ).attr( 'data-pecentage' );

                    $( this ).attr( 'style', 'left:' + i + '' );

                    if( $( this ).hasClass( 'active' ) ){

                        $( '.inside' ).attr( 'style', 'width:'+i );
                    }

                } );
            }

            $( '#timeline .dot' ).on( 'click', function(e){

              var id = $( this ).attr( 'id' ),
                  persentage = $( this ).attr( 'data-pecentage' );

                  if( $( '.dot' ).length ){

                      $( '.dot' ).map( function(){

                          $( this ).removeClass( 'active' );

                      } );

                      $( this ).addClass( 'active' );
                  }

              if( $( '.history-content' ).length ){

                  $( '.history-content' ).map( function(){

                        $( this ).addClass( 'd-none' );
                  } );
              }

              $( '.'+id ).removeClass( 'd-none' );

              $( '.inside' ).attr( 'style', 'width:'+persentage );

              e.preventDefault();

            } );
        }
    },

    contactus_map: function(){

        if( $('#map_canvas').length ){    

            var mylatlongs = [
                {
                    "id"        : 1, 
                    "heading"   : "Plumart, Melbourne",
                    "address"   : '121 King Street, Melbourne Victoria 3000 Australia',
                    "email"     : 'mail@plumart.com',
                    "contact"   : '+61 3 8376 6284',
                    "lat"       : "40.761335",  
                    "lng"       : "-73.916456", 
                    "image"     : './images/marker/store.png',
                    "icon"      : './images/marker/marker.png'
                },
            ];

            var infowindow = null;
            jQuery(function() {
                    var StartLatLng = new google.maps.LatLng(mylatlongs[0]['lat'], mylatlongs[0]['lng']);
                    var mapOptions = {
                        center: StartLatLng,
                        streetViewControl: false,
                        panControl: false,
                        maxZoom:16,
                        zoom : 12,
                        zoomControl:true,
                        zoomControlOptions: {
                            style:google.maps.ZoomControlStyle.SMALL
                        },
                        mapTypeId: google.maps.MapTypeId.ROADMAP,

                    };

                var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

                var infowindow = new google.maps.InfoWindow({
                    content: ''
                });
                
                jQuery.each( mylatlongs, function(i, m) {
                    var StartLatLng = new google.maps.LatLng(-25.901820984476252, 134.00135040279997);

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(m.lat, m.lng),
                        bounds: true,
                        id : 'mk_' + m.id,
                        icon : m.icon,
                        letter : m.index,
                        map: map,
                        title: m.heading
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.close();
                        infowindow.setContent(contentString);
                        infowindow.open(map,marker);
                    });

                    var contentString = 

                    '<div class="plumbo-infowindow">'+

                         '<div class="image-box">'+
                            '<img src="'+ m.image +'">'+
                            '<h5 class="heading text-center">'+ m.heading + '</h5>'+
                            '<p class="address text-center">'+ m.address + '</p>'+
                         '</div>'+

                         '<ul class="contact-box">'+
                            '<li class="email"><a href="mailto:'+m.email+'">'+m.email+'</a></li>'+
                            '<li class="contact"><a href="tel:'+m.contact+'">'+m.contact+'</a></li>'+
                         '</ul>'+

                      '</div>';

                });
            });

        }
    },

    number_counter: function(){

      if( $('.plumart-counter .counter').length ){

        $('.plumart-counter .counter').map( function(){

            $(this).animate({
                counter: $( this ).attr( 'data-counter' )
            }, {
                duration: 6000,
                easing: 'swing',
                step: function(now) {
                    $(this).text(Math.ceil(now));
                },
                complete: wporganic.number_counter
            });

        } );
      }
    },

    popup_gallery: function(){

        if( $('.image-popup-vertical-fit').length ){

            $('.image-popup-vertical-fit').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-img-mobile',
                image: {
                  verticalFit: true
                }
            });
        }
    },

    menu: function(){

        if( $("#cssmenu").length ){

            $("#cssmenu").menumaker({
                title: "Menu",
                format: "multitoggle"
            });
        }
    },

    back_to_top: function(){

      if( $('#back-to-top').length ){

        $(window).scroll(function(){

            if ($(this).scrollTop() > 100) {

                $('#back-to-top').fadeIn();

            } else {

                $('#back-to-top').fadeOut();
            }
        });

        $('#back-to-top').click(function(){

              $('body,html').animate({ scrollTop: 0 }, 800);
              return false;
        });
      }
    },

    datepicker: function(){

        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
        });
        $('.datepicker').datepicker("setDate", new Date());

    },

    initializ: function(){

        this.slider_one();
        this.tabs( '.plumbo-tab-service ul.nav-tabs li' );
        this.tabs( '.testimonial-section ul.nav-tabs li' );
        this.footer_gallery();
        this.history_tab();
        this.contactus_map();
        this.number_counter();
        this.popup_gallery();
        this.menu();
        this.datepicker();
        this.back_to_top();
    }

  };

  /* ---------------------------------------------
   Document ready function
   --------------------------------------------- */
  $(function() {

      wporganic.loader();
      wporganic.initializ();
  });

})(jQuery);